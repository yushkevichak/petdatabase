/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.lifecycle;

import javax.inject.Inject;
import io.quarkus.runtime.StartupEvent;
import javax.enterprise.event.Observes;
import ru.ael.petdb.entity.Pet;
import ru.ael.petdb.petservice.PetService;
import ru.ael.petdb.petservice.PetService;

/**
 *
 * @author student
 */
public class LifeCycle {

    @Inject
    PetService sgServ;

    void onStart(@Observes StartupEvent event) {

        Pet pet = new Pet();
        pet.setИмя("Снежок");
        sgServ.savePet(pet);
    }

}
