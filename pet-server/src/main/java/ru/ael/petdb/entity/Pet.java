/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author student
 *
 */
@Entity
@Table(name = "pet")

public class Pet {

    private String имя;
    private String порода;
    private String возраст;
    
    @Id
    @SequenceGenerator(name = "studentSeq", sequenceName = "student_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "studentSeq")
    private Long id;

    public Long getId(Long id) {
       return id;
    }
    
public void setId(Long id) {
        this.id = id;
    }

public String getИмя() {
    return имя;
}
public void setИмя(String имя) {
    this.имя = имя;
}
public String getПорода() {
    return порода;
}
public void setПорода(String порода) {
        this.порода = порода;
    }
public String getВозраст() {
    return возраст;
}
public void setВозраст(String возраст) {
        this.возраст = возраст;
    }
}
