/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.petservice;

import javax.inject.Inject;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.ael.petdb.entity.Pet;

/**
 *
 * @author student
 */
@ApplicationScoped

public class PetService {

    @Inject
    EntityManager em;

    @Transactional
    public Pet savePet(Pet pet) {
        em.persist(pet);
        return pet;
    }
}
